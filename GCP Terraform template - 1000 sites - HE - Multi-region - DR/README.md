﻿This Terraform Template is intended to Automate Versa Head End deployment on Google Cloud Platform (GCP) in HA mode (DC - Region 1 & DR - Region 2) 

- **Pre-requisites for using this template:** 
1. \*\*Terraform Install:\*\* To Download & Install Terraform to the same directory as terraform scripts, refer Link "www.terraform.io/downloads.html" 
1. \*\*Terraform Setup for GCP:\*\* To Setup Terraform for Google Cloud Account, first create a service account in google cloud account, refer link "https://cloud.google.com/iam/docs/creating-managing- service-accounts#creating". 
1. Then create service account keys in json file format for the service account created above, refer link "https://cloud.google.com/iam/docs/creating-managing-service- account-keys#creating\_service\_account\_keys". 
1. Here you will get one json file for the service account which will be used to authenticate towards google cloud platform. Download this JSON file to the same directory as terraform scripts. 
1. \*\*Versa Head End Images:\*\* Image available in your google cloud account for: 
   1. Versa Director 
   1. Versa Flex-VNF (Controller/Service VNF) 
   1. Versa Analytics 
1. Create SSH key pair by following this article - https://cloud.google.com/compute/docs/instances/adding-removing- ssh-keys#windows & paste the public key in id\_rsa file under terraform scripts directory. This is used to take remote ssh access of the devices using GCP VM instance Custom metadata . Same key pair can be used for **ssh\_key** field in **terraform.tfvars file.** 
- **Usage**: 
- Download all the files in PC. It is recommended that place all the files in new/separate folder as terraform will store the state file for this environment once it is applied. 
- Go to the folder where all the required files are placed using command prompt. 
- Use command  terraform init  to initialize. it will download necessary terraform plugins required to run this template. 
- Then use command  terraform plan  to plan the deployment. It will show the plan regarding all the resources being provisioned as part of this template. 
- At last use command  terraform apply  to apply this plan in action for deployment. It will start deploying all the resource on Google Cloud. 

**Note: DR site should be deployed only when DC site is completely deployed.** 

**It will require below files to run it.** 

- main.tf file. 
- var.tf file. 
- terraform.tfvars file. 
- output.tf file. 
- director.sh file. 
- script.sh file. 
- credentials.json file  
- id\_rsa  

***Note*** : Once DC and DR setup are up, configure  

1. DC Director with a static route - **Destination:** DR site management segment 

**Next Hop**   : DC Director Eth 2 Interface segment gateway (eg if eth2 Ip is 10.41.9.11/24 then next hop should be 10.41.9.1 ) 

**via interface** : eth2 

2. DR Director with a static route - **Destination**: DC site management segment 

**Next Hop**   : DR Director Eth 2 Interface segment gateway (eg if eth2 Ip is 10.21.9.11/24 then next hop should be 10.21.9.1 ) 

**via interface** : eth2 

**\*\*terraform.tfvars file:\*\* : User Inputs** 

terraform.tfvars file is being used to get the variables values from user. User need to update the required fields according to their environment. This file will have below information: 

- credentials\_file : Provide the credential file(Pre-requisites- step2) path which was obtained during initial setup process for service account. 
- project\_id : Provide the google cloud project id where head end has to be deployed. 
- region : Provide the region where user wants to deploy Versa HE setup. E.g. us-west1, us-central1 etc. 
- zone : Provide the zone from the selected region above where all the resources belonging to Versa HE Setup will be created. E.g. us- west1-a, us-central1-a etc. 
- ssh\_key : Provide the ssh public key information here. Use this link to generate new ssh key https://cloud.google.com/compute/docs/instances/adding-removing- ssh-keys#windows. This key will be injected into instances which can be used to login into instances later on. User can generate the ssh key using "sshkey-gen" or "putty key generator" tool to generate the ssh keys. Here Public key information is required. 
- customer\_name : Provide customer name -> follow this link for naming convention https://cloud.google.com/compute/docs/naming- resources 
- ip\_cidr\_range : Provide the address space information to create the subnetwork in Google in each network for DR - headend infra 
- destination\_ip\_range : Provide the destination IP range for which Custom route will be added to use controller address as a gateway for the IPSec overlay network address for the netconf traffic originating from Director. This will be SDWAN overlay route. 
- dc\_cidr : Provide the address space information to create the subnetwork in Google in each network for DC - headend infra 
- noc\_segment : Provide source segment from which ssh & https access will be allowed to all the devices - via management interface from Internet 
- labels : Provide the labels/tags to be added to the instances. 
- versa\_director\_image : Provide the Versa Director Image name. 
- versa\_flexvnf\_image : Provide the Versa Controller Image name. 
- hostname\_director : Provide the hostname for Versa Director to be used.  
- machine\_type\_dir : Provide the machine type/size which will be used to provision the Versa Director Instance. By default, n1-standard-4 will be used. 
- machine\_type\_controller : Provide the machine type/size which will be used to provision the Versa Controller Instance. By default, n1- standard-4 will be used. 
- machine\_type\_svnf : Provide the machine type/size which will be used to provision the Versa Service VNF. By default, n1-standard-4 will be used 
- mgmt\_ip : Provide static internal(private) IPs to be used for management  
- sb : Provide static internal(private) IPs to be used for southbound connectivity 
- svnf\_svnf : Provide static internal(private) IP to be used for DC - DR service VNF interconnection 
- svnf\_ctrl : Provide static internal(private) IPs to be used for Service VNF - Controller connectivity 
- internet : Provide static internal(private) IP to be used for Controller Internet transport 
- wan : Provide static internal(private) IP to be used for Controller WAN transport 
- interconnect : Provide static internal(private) IP to be used for DC - DR Northbound reachability ( DR - Director Eth2 IP) 
- vdeth0 : Provide static internal(private) IP that will be used for DR - site Director management with subnet mask.(This is required to create routes in GCP route table needed to bring up the infra) 
- dc\_vd\_mgmt : Provide static internal(private) IP that will be used for DC site director management 
- dc\_vd\_interconnect : Provide static internal(private) IP to be used for DC - DR Northbound reachability ( DC - Director Eth2 IP) 
