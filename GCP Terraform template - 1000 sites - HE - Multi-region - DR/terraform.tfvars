#----------------------------------------------------------------------
# Variable's value defined here
#----------------------------------------------------------------------

credentials_file        = "versa-system-engineering-9c0c15909f0a.json"
project_id              = "versa-sys-engineering"
region                  = "us-east4"
zone                    = "us-east4-a"
ssh_key                 = "ssh-rsa AAAAB3NzaC1yc26+9jPDU+DU0Dgjj5Iborm8F75TtAxdKYcqvbZQZqUHQaLxZPexFZbbWNGEoTQzYJfqkXPkmkOyMn8kSjAKcvKWm3Luf6TmdELU5qCRKdCukeXTJiLnWdy10ALQm7FLkzqoyJxVEIkFWX8bqLdsN8/TyE8tjUPSCeT7qgB9MesOcVIV5PEZK4tbN0zTLhcNkXz6PAjLp+sMYxMI37pWFfepaZqylmYBEw5mKAwnjvPgVk9ejJLZCZDLLfMyY+ufrdEFlUCjwoixrEOqZIzfm55RBPz3dhUXP41qNMw1rnegFNFACps5V+bnn3v6P3fiL84G4g+NGf1vdbDo7TsZHSXjYRKbIcfht800OUa+46XZk5MILUQ/p8QfL7bgYt35yaxaDGXDyLTo1evQRtKuMTjjc90DZVRoX610XkN/CG+OL9AmsOjmt7unyCfnkFIjyZvk= admin"
ip_cidr_range           = ["10.41.1.0/24", "10.41.2.0/24", "10.41.4.0/24", "10.41.5.0/24", "10.41.6.0/24", "10.41.7.0/24", "10.41.9.0/24" ]
dc_cidr                 = ["10.21.1.0/24", "10.21.2.0/24", "10.21.3.0/24", "10.21.4.0/24", "10.21.5.0/24", "10.21.6.0/24", "10.21.7.0/24", "10.21.9.0/24"  ]
destination_ip_range    = "10.10.0.0/16"
noc_segment             = ["0.0.0.0/0"]  
customer_name           = "customer-1"
versa_director_image    = "versa-director-cc0012d-21-2-1"
versa_flexvnf_image     = "versa-flexvnf-5bed617-21-2-1"
labels                  = { "infra" = "versa-dr-he" }
hostname_director       = "dr-director-1"
machine_type_dir        = "n1-standard-4"
machine_type_svnf       = "n1-standard-4"
machine_type_controller = "n1-standard-4"
mgmt_ip                 = ["10.41.1.11", "10.41.1.12", "10.41.1.13", "10.41.1.14", "10.41.1.15"]
sb                      = ["10.41.2.11", "10.41.2.12", "10.41.2.13", "10.41.2.14"]
svnf_svnf               = ["10.41.4.11"]
svnf_ctrl               = ["10.41.5.11", "10.41.5.12"]
internet                = ["10.41.6.11"]
wan                     = ["10.41.7.11"]
interconnect            = ["10.21.9.11"]
vdeth0                  = "10.41.1.11/32"
dc_vd_mgmt              = ["10.21.1.11"]
dc_vd_interconnect      = ["10.41.9.11"]