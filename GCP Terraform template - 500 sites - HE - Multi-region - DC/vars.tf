variable "credentials_file" {
  description = "Credential file in json format for the service account which will be used to authenticate and provision the required resources."
}

variable "project_id" {
  description = "Google Project ID where Versa head end has to be deployed."
}

variable "region" {
  description = "Region name where Versa head end has to be deployed."
}

variable "ssh_key" {
  description = "SSH Key to be injected into VMs deployed on GCE required for login into instance later on."
}

variable "zone" {
  description = "zone name where Versa head end has to be deployed in a particular region."
}

variable "ip_cidr_range" {
  description = "Provide the list of IP address range required for Mangement, Control, WAN & LAN network."
  default     = ["10.231.1.0/24", "10.231.2.0/24", "10.231.3.0/24", "10.231.4.0/24", "10.231.5.0/24", "10.231.6.0/24", "10.231.7.0/24", "10.231.9.0/24"]
}

variable "destination_ip_range" {
  description = "Destination IP CIDR range to add custom route. This will be SDWAN overlay route"
  default     = "10.10.0.0/16"
}

variable "manage" {
   default    = ["10.21.1.11", "10.21.1.12", "10.21.1.13", "10.21.1.14", "10.21.1.15", "10.21.1.16", "10.21.1.17"]
}

variable "sb_segment" {
   default    = ["10.21.2.11", "10.21.2.12", "10.21.2.13", "10.21.2.14", "10.21.2.15", "10.21.2.16"]
}

variable "van_segment" {
   default    = ["10.21.3.11", "10.21.3.12", "10.21.3.13", "10.21.3.14"]
}

variable "svnf_svnf" {
   default    = ["10.21.4.11"]
}

variable "svnf_ctrl" {
   default    = ["10.21.5.11", "10.21.5.12"]
}

variable "internet" {
   default    = ["10.21.6.11"]
}

variable "wan" {
   default    = ["10.21.7.11"]
}

variable "interconnect" {
   default    = ["10.232.9.11"]
}

variable "dr_vd_mgmt" {
   default    = ["10.41.1.11"]
}

variable "dr_vd_interconnect" {
   default    = ["10.21.9.11"]
}

variable "dr_cidr" {
  default     = ["10.232.1.0/24", "10.232.2.0/24", "10.232.3.0/24", "10.232.4.0/24", "10.232.5.0/24", "10.232.6.0/24", "10.232.7.0/24", "10.232.9.0/24" ]
}

variable "noc_segment" {
  default     = ["0.0.0.0/0"]
}

variable "customer_name" {
  default     = "customer1"
}

variable "versa_director_image" {
  description = "Provide the name of Versa Director Image."
}

variable "versa_van_image" {
  description = "Provide the name of Versa Analytics Image."
}

variable "versa_flexvnf_image" {
  description = "Provide the name of Versa FlexVNF Image."
}

variable "labels" {
  description = "Tags/Lavels to be added to the VMs."
  default     = { "infra" = "versa-headend" }
}

variable "hostname_director" {
  description = "Hostname of the Director instance."
  default     = "versa-director-gce"
}

variable "hostname_van" {
  description = "Hostname of the Analytics instance."
  type        = list(string)
  default = [
    "versa-analytics-1",
    "versa-analytics-2",
  ]
}

variable "machine_type_dir" {
  description = "Provide the machine type (size) for Director instance to be used."
  default     = "n1-standard-4"
}

variable "machine_type_van" {
  description = "Provide the machine type (size) for Analytics instance to be used."
  default     = "n1-standard-4"
}

variable "machine_type_controller" {
  description = "Provide the machine type (size) for Controller instance to be used."
  default     = "n1-standard-4"
}

variable "machine_type_svnf" {
  description = "Provide the machine type (size) for svnf instance to be used."
  default     = "n1-standard-4"
}

variable "vos_fw_count" {
  default     = [
  "4",
  "5",
  "6"
  ]
}

