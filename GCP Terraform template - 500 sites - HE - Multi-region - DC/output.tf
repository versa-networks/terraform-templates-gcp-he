output "director_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_director.network_interface[0].access_config.0.nat_ip
}

output "controller_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_controller.network_interface[0].access_config.0.nat_ip
}

output "SVNF_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_svnf.network_interface[0].access_config.0.nat_ip
}

output "Analytics1_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[0].network_interface[0].access_config.0.nat_ip
}

output "Analytics2_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[1].network_interface[0].access_config.0.nat_ip
}


