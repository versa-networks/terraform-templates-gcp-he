# Configure the Google Cloud provider
provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project_id
  region      = var.region
}

# Add check for terraform required version
terraform {
  required_version = ">=0.13, <0.14"
  required_providers {
    google   = "<4.0,>= 2.12"
    random   = "~> 3.0"
    template = "~> 2.2"
  }
}

# Generate random text
resource "random_id" "randomId" {
  byte_length = 4
}
# Add template to use custom data for Director:
data "template_file" "user_data_director" {
  template = file("director.sh")
  

  vars = {
    hostname_dir   = var.hostname_director
	hostname_van_1 = var.hostname_van[0]
	hostname_van_2 = var.hostname_van[1]
	hostname_van_3 = var.hostname_van[2]
	hostname_van_4 = var.hostname_van[3]
    sshkey         = var.ssh_key
	
  }
  
}

# Add template to use custom data for Analytics:
data "template_file" "user_data_van" {
  count    = length(var.hostname_van)
  template = file("script.sh")

  vars = {
    hostname_van = var.hostname_van[count.index]
    dir_mgmt_ip  = local.director_mgmt_ip
	dr_vd_mgmt  = var.dr_vd_mgmt[0]
	dr_vd_interconnect = var.dr_vd_interconnect[0]
    sshkey       = var.ssh_key
  }
}

# Add template to use custom data for Controller:
data "template_file" "user_data_vnf" {
  template = file("script.sh")

  vars = {
    dir_mgmt_ip  = local.director_mgmt_ip
	dr_vd_mgmt  = var.dr_vd_mgmt[0]
	dr_vd_interconnect = var.dr_vd_interconnect[0]
    sshkey       = var.ssh_key
  }
}

# Add template to use custom data for ServiceVNF:
data "template_file" "user_data_svnf" {
  template = file("script.sh")

  vars = {
    dir_mgmt_ip  = local.director_mgmt_ip
	dr_vd_mgmt  = var.dr_vd_mgmt[0]
	dr_vd_interconnect = var.dr_vd_interconnect[0]
    sshkey       = var.ssh_key
  }
}

locals {
  vpc_name           = ["mgmt", "control", "van-internal", "svnf-svnf", "svnf-ctrl", "internet", "wan"]
  controller_ctrl_ip = google_compute_instance.versa_vm_controller.network_interface[1].network_ip
  director_mgmt_ip   = google_compute_instance.versa_vm_director.network_interface[0].network_ip
  svnf_ctrl_ip       = google_compute_instance.versa_vm_svnf.network_interface[1].network_ip
  svnf_ctrller       = google_compute_instance.versa_vm_svnf.network_interface[3].network_ip
  
}

# Create VPC network for Head end deployment
resource "google_compute_network" "vpc_network" {
  count                           = 7
  name                            = "dc-vpc-network-${local.vpc_name[count.index]}-${var.customer_name}"
  auto_create_subnetworks         = false
  routing_mode                    = "REGIONAL"
  delete_default_routes_on_create = false
}

# Create VPC network for DR Mgmt
resource "google_compute_network" "dc_dr_network" {
  count                           = 1
  name                            = "dr-vpc-network-${local.vpc_name[count.index]}-${var.customer_name}"
  auto_create_subnetworks         = false
  routing_mode                    = "REGIONAL"
  delete_default_routes_on_create = false
}

# Create one subnet in dr mgmt vpc network for head end deployment
resource "google_compute_subnetwork" "dc_dr_mgmt" {
  count                    = 1
  name                     = "dc-dr-vpc-${local.vpc_name[count.index]}-${var.customer_name}"
  ip_cidr_range            = var.dr_cidr[7]
  private_ip_google_access = "false"
  network                  = google_compute_network.dc_dr_network[0].name
}


# Create one subnet in each vpc network for head end deployment
resource "google_compute_subnetwork" "subnet_mgmt" {
  count                    = 7
  name                     = "dc-vpc-subnet-${local.vpc_name[count.index]}-${random_id.randomId.hex}"
  ip_cidr_range            = element(var.ip_cidr_range, count.index)
  private_ip_google_access = "false"
  network                  = google_compute_network.vpc_network[count.index].name
}

resource "google_compute_address" "internal_mgmt" {
  count        = 7
  name         = "dc-mgmt-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[0].id
  address_type = "INTERNAL"
  address      = var.manage[count.index]
}

resource "google_compute_address" "internal_sb" {
  count        = 6
  name         = "dc-sb-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[1].id
  address_type = "INTERNAL"
  address      = var.sb_segment[count.index]
}

resource "google_compute_address" "internal_van" {
  count        = 4
  name         = "dc-van-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[2].id
  address_type = "INTERNAL"
  address      = var.van_segment[count.index]
}

resource "google_compute_address" "internal_svnf_svnf" {
  count        = 1
  name         = "dc-svnf-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[3].id
  address_type = "INTERNAL"
  address      = var.svnf_svnf[count.index]
}

resource "google_compute_address" "internal_svnf_ctrl" {
  count        = 2
  name         = "dc-sc-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[4].id
  address_type = "INTERNAL"
  address      = var.svnf_ctrl[count.index]
}

resource "google_compute_address" "internal_internet" {
  count        = 1
  name         = "dc-int-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[5].id
  address_type = "INTERNAL"
  address      = var.internet[count.index]
}

resource "google_compute_address" "internal_wan" {
  count        = 1
  name         = "dc-wan-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.subnet_mgmt[6].id
  address_type = "INTERNAL"
  address      = var.wan[count.index]
}

resource "google_compute_address" "interconnect" {
  count        = 1
  name         = "dc-dr-internal-address-${count.index}"
  subnetwork   = google_compute_subnetwork.dc_dr_mgmt[0].id
  address_type = "INTERNAL"
  address      = var.interconnect[count.index]
}


# Create firewall rule for ssh/https for Management Network
resource "google_compute_firewall" "ssh_https_versa_mgmt" {
  name          = "ssh-https-dc-fw-mgmt-net-${random_id.randomId.hex}"
  network       = google_compute_network.vpc_network[0].name
  priority      = 100
  source_ranges = var.noc_segment
  allow {
    protocol = "tcp"
    ports    = ["22", "443"]
  }


}

# Create firewall rule for Versa HE deplooyment for Management Network
resource "google_compute_firewall" "firewall_versa_mgmt" {
  name     = "dc-mgmt-net-firewall-${random_id.randomId.hex}"
  network  = google_compute_network.vpc_network[0].name
  priority = 250
  
  source_ranges = concat(var.dr_cidr, var.ip_cidr_range)
  

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "8080", "8443", "1234", "8010", "8020", "8983", "4566", "4570", "5432", "9182-9183", "2022", "20514", "6080", "9090"]
  }

  allow {
    protocol = "udp"
    ports    = ["53", "123", "20514"]
  }
}

# Create firewall rule for Versa HE deplooyment for Control Network (Director - Controller - Analytics)
resource "google_compute_firewall" "firewall_versa_ctrl" {
  name     = "dc-sb-network-firewall-${random_id.randomId.hex}"
  network  = google_compute_network.vpc_network[1].name
  priority = 250

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "7000-7001", "7199", "8983", "9042", "9160", "9162", "2181", "2888", "3888", "1234", "5000", "5010", "8008", "4566", "4570", "5432", "9182", "9183", "2022", "20514"]
  }

  allow {
    protocol = "udp"
    ports    = ["53", "123", "20514"]
  }
}

# Create firewall rule for Versa HE deplooyment for VAN-Internal Network
resource "google_compute_firewall" "firewall_versa_van" {
  name     = "analytics-internal-firewall-${random_id.randomId.hex}"
  network  = google_compute_network.vpc_network[2].name
  priority = 250

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "7000-7001", "7199", "8983", "9042", "9160", "9162", "2181", "2888", "3888", "5000", "5010", "8008"]
  }

  allow {
    protocol = "udp"
    ports    = ["53", "123"]
  }
}

# Create firewall rule for SVNF-SVNF 
resource "google_compute_firewall" "firewall_svnf_svnf" {
  name     = "dc-svnf-interconnect-fw-${random_id.randomId.hex}"
  network  = google_compute_network.vpc_network[3].name
  priority = 250
  
  allow {
    protocol = "icmp"
  }
  
  allow {
    protocol = "udp"
    ports    = ["500", "4500", "4790"]
  }

  allow {
    protocol = "esp"
  }
  
}

# Create firewall rule for Versa HE deplooyment for VOS
resource "google_compute_firewall" "firewall_versa_vos" {
  count    = 3
  name     = "dc-vos-fw-${count.index}-${random_id.randomId.hex}"
  network  = google_compute_network.vpc_network[var.vos_fw_count[count.index]].name
  priority = 250

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "2022", "1024-1120", "3000-3003", "9878", "8443", "5201", "179", "1234"]
  }

  allow {
    protocol = "udp"
    ports    = ["53", "123", "500", "3002-3003", "4500", "4790", "1234"]
  }

  allow {
    protocol = "esp"
  }
  
}


# Create Public IP for Management Interface for Director
resource "google_compute_address" "pub_ip_dir_mgmt" {
  count = 1
  name  = "pub-ip-dir-mgmt-${count.index}-${random_id.randomId.hex}"
}

# Create Public IP for Management Interface for Controller
resource "google_compute_address" "pub_ip_ctrl_mgmt" {
  count = 1
  name  = "pub-ip-ctrl-mgmt-${count.index}-${random_id.randomId.hex}"
}

# Create Public IP for Management Interface for SVNF
resource "google_compute_address" "pub_ip_svnf_mgmt" {
  count = 1
  name  = "pub-ip-svnf-mgmt-${count.index}-${random_id.randomId.hex}"
}

# Create Public IP for Management Interface for Analytics
resource "google_compute_address" "pub_ip_van_mgmt" {
  count = 4
  name  = "pub-ip-van-mgmt-${count.index}-${random_id.randomId.hex}"
}

# Create Public IPs for WAN Interface for Controller
resource "google_compute_address" "pub_ip_wan" {
  count = 3
  name  = "pub-ip-wan-ctrl-${count.index}-${random_id.randomId.hex}"
}


# Create custom route for overlay from SVNF to controller
resource "google_compute_route" "route_1" {
  name        = "overlay-svnf-ctrl-${random_id.randomId.hex}"
  dest_range  = var.destination_ip_range
  network     = google_compute_network.vpc_network[4].name
  next_hop_ip = local.controller_ctrl_ip
}

#Create custom route for overlay from Director to SVNF
resource "google_compute_route" "route_2" {
  name        = "overlay-vd-svnf-${random_id.randomId.hex}"
  dest_range  = var.destination_ip_range
  network     = google_compute_network.vpc_network[1].name
  next_hop_ip = local.svnf_ctrl_ip
}

#Create custom route for svnf-ctrl segment from Director to SVNF
resource "google_compute_route" "route_3" {
  name        = "dc-controller-${random_id.randomId.hex}"
  dest_range  = var.ip_cidr_range[4]
  network     = google_compute_network.vpc_network[1].name
  next_hop_ip = local.svnf_ctrl_ip
}

#Create custom route for dr-sb segment from Director to SVNF
resource "google_compute_route" "route_4" {
  name        = "dr-sb-${random_id.randomId.hex}"
  dest_range  = var.dr_cidr[1]
  network     = google_compute_network.vpc_network[1].name
  next_hop_ip = local.svnf_ctrl_ip
}

#Create custom route for  dr svnf-ctrl segment from Director to SVNF
resource "google_compute_route" "route_5" {
  name        = "dr-controller-${random_id.randomId.hex}"
  dest_range  = var.dr_cidr[4]
  network     = google_compute_network.vpc_network[1].name
  next_hop_ip = local.svnf_ctrl_ip
}

# Create custom route for dc director via SVNF
resource "google_compute_route" "route_6" {
  name        = "dc-director-sb-${random_id.randomId.hex}"
  dest_range  = var.ip_cidr_range[1]
  network     = google_compute_network.vpc_network[4].name
  next_hop_ip = local.svnf_ctrller
}

# Create custom route for dr director via SVNF
resource "google_compute_route" "route_7" {
  name        = "dr-director-sb-${random_id.randomId.hex}"
  dest_range  = var.dr_cidr[1]
  network     = google_compute_network.vpc_network[4].name
  next_hop_ip = local.svnf_ctrller
}

# Create Director Instance
resource "google_compute_instance" "versa_vm_director" {
  name                      = "${var.hostname_director}"
  zone                      = var.zone
  allow_stopping_for_update = true
  can_ip_forward            = true
  labels                    = var.labels
  machine_type              = var.machine_type_dir

  boot_disk {
    initialize_params {
      image = var.versa_director_image
	  size  = 512
	  type  = "pd-ssd"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[0].name
	network_ip = google_compute_address.internal_mgmt[0].address
    access_config {
      nat_ip = google_compute_address.pub_ip_dir_mgmt[0].address
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[1].name
	network_ip = google_compute_address.internal_sb[0].address
	
  }
  
  network_interface {
    subnetwork = google_compute_subnetwork.dc_dr_mgmt[0].name
	network_ip = google_compute_address.interconnect[0].address
	
  }
  
  metadata_startup_script = data.template_file.user_data_director.rendered
  metadata = {
  ssh-keys = "admin:${file("id_rsa.pub")}"
}
}

# Create Analytics Instance
resource "google_compute_instance" "versa_vm_van" {
  count                     = 4
  name                      = "${var.hostname_van[count.index]}"
  zone                      = var.zone
  allow_stopping_for_update = true
  can_ip_forward            = true
  labels                    = var.labels
  machine_type              = var.machine_type_van

  boot_disk {
    initialize_params {
      image = var.versa_van_image
	  size  = 2000
	  type  = "pd-ssd"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[0].name
	network_ip = google_compute_address.internal_mgmt[1+count.index].address
    access_config {
      nat_ip = google_compute_address.pub_ip_van_mgmt[count.index].address
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[1].name
	network_ip = google_compute_address.internal_sb[1+count.index].address
  }
  
  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[2].name
	network_ip = google_compute_address.internal_van[count.index].address
	
  }
  metadata = {
  ssh-keys = "admin:${file("id_rsa.pub")}"
}
  

  metadata_startup_script = data.template_file.user_data_van[count.index].rendered
}

# Create Controller Instance
resource "google_compute_instance" "versa_vm_controller" {
  name                      = "${var.customer_name}-dc-controller"
  zone                      = var.zone
  allow_stopping_for_update = true
  can_ip_forward            = true
  labels                    = var.labels
  machine_type              = var.machine_type_controller

  boot_disk {
    initialize_params {
      image = var.versa_flexvnf_image
	  size  = 128
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[0].name
	network_ip = google_compute_address.internal_mgmt[5].address
    access_config {
      nat_ip = google_compute_address.pub_ip_ctrl_mgmt[0].address
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[4].name
	network_ip = google_compute_address.internal_svnf_ctrl[1].address
	
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[5].name
	network_ip = google_compute_address.internal_internet[0].address
    access_config {
      nat_ip = google_compute_address.pub_ip_wan[0].address
    }
  }
  
  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[6].name
	network_ip = google_compute_address.internal_wan[0].address
    access_config {
      nat_ip = google_compute_address.pub_ip_wan[1].address
    }
  }
  
  metadata = {
  ssh-keys = "admin:${file("id_rsa.pub")}"
}

  metadata_startup_script = data.template_file.user_data_vnf.rendered
}

# Create SVNF Instance
resource "google_compute_instance" "versa_vm_svnf" {
  name                      = "${var.customer_name}-dc-service-vnf"
  zone                      = var.zone
  allow_stopping_for_update = true
  can_ip_forward            = true
  labels                    = var.labels
  machine_type              = var.machine_type_svnf

  boot_disk {
    initialize_params {
      image = var.versa_flexvnf_image
	  size  = 128
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[0].name
	network_ip = google_compute_address.internal_mgmt[6].address
    access_config {
      nat_ip = google_compute_address.pub_ip_svnf_mgmt[0].address
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[1].name
	network_ip = google_compute_address.internal_sb[5].address
	
  }
  
  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[3].name
	network_ip = google_compute_address.internal_svnf_svnf[0].address
    access_config {
      nat_ip = google_compute_address.pub_ip_wan[2].address
    }
  }
  
  network_interface {
    subnetwork = google_compute_subnetwork.subnet_mgmt[4].name
	network_ip = google_compute_address.internal_svnf_ctrl[0].address
  }

  metadata = {
  ssh-keys = "admin:${file("id_rsa.pub")}"
}
  metadata_startup_script = data.template_file.user_data_svnf.rendered
}



