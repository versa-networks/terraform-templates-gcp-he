output "director_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_director.network_interface[0].access_config.0.nat_ip
}

output "controller_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_controller.network_interface[0].access_config.0.nat_ip
}

output "SVNF_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_svnf.network_interface[0].access_config.0.nat_ip
}

output "Analytics1_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[0].network_interface[0].access_config.0.nat_ip
}

output "Analytics2_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[1].network_interface[0].access_config.0.nat_ip
}

output "Analytics3_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[2].network_interface[0].access_config.0.nat_ip
}

output "Analytics4_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[3].network_interface[0].access_config.0.nat_ip
}

output "Analytics5_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[4].network_interface[0].access_config.0.nat_ip
}

output "Analytics6_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_van[5].network_interface[0].access_config.0.nat_ip
}

output "LC1_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_vlc[0].network_interface[0].access_config.0.nat_ip
}

output "LC2_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_vlc[1].network_interface[0].access_config.0.nat_ip
}

output "LC3_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_vlc[2].network_interface[0].access_config.0.nat_ip
}

output "LC4_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_vlc[3].network_interface[0].access_config.0.nat_ip
}

output "LC5_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_vlc[4].network_interface[0].access_config.0.nat_ip
}

output "LC6_mgmt_public_ip" {
  value = google_compute_instance.versa_vm_vlc[5].network_interface[0].access_config.0.nat_ip
}


