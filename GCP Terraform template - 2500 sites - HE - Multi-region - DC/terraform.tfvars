#----------------------------------------------------------------------
# Variable's value defined here
#----------------------------------------------------------------------

credentials_file        = "versa-system-engineering-9c0c15909f0a.json"
project_id              = "versa-sys-engineering"
region                  = "asia-east1"
zone                    = "asia-east1-a"
ssh_key                 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAu4UBIG96+9jPDU+DU0Dgjj5Iborm8F75TtAxdKYcqvbZQZqUHQaLxZPexFZbbWNGEoTQzYJfqkXPkmkOyMn8kSjAKcvKWm3Luf6TmdELU5qCRKdCukeXTJiLnWdy10ALQm7FLkzqoyJxVEIkFWX8bqLdsN8/TyE8tjUPSCeT7qgB9MesOcVIV5PEZK4tbN0zTLhcNkXz6PAjLp+sMYxMI37pWFfepaZqylmYBEw5mKAwnjvPgVk9ejJLZCZDLLfMyY+ufrdEFlUCjwoixrEOqZIzfm55RBPz3dhUXP41qNMw1rnegFNFACps5V+bnn3v6P3fiL84G4g+NGf1vdbDo7TsZHSXjYRKbIcfht800OUa+46XZk5MILUQ/p8QfL7bgYt35yaxaDGXDyLTo1evQRtKuMTjjc90DZVRoX610XkN/CG+OL9AmsOjmt7unyCfnkFIjyZvk= admin"
customer_name           = "customer-1"
ip_cidr_range           = ["10.21.1.0/24", "10.21.2.0/24", "10.21.3.0/24", "10.21.4.0/24", "10.21.5.0/24", "10.21.6.0/24", "10.21.7.0/24", "10.21.9.0/24"  ]
dr_cidr                 = ["10.41.1.0/24", "10.41.2.0/24", "10.41.3.0/24", "10.41.4.0/24", "10.41.5.0/24", "10.41.6.0/24", "10.41.7.0/24", "10.41.9.0/24" ]
destination_ip_range    = "10.10.0.0/16"
noc_segment             = ["0.0.0.0/0"]
versa_director_image    = "versa-director-cc0012d-21-2-1"
versa_van_image         = "versa-analytics-5bed617-21-2-1"
versa_flexvnf_image     = "versa-flexvnf-5bed617-21-2-1"
labels                  = { "infra" = "versa-dc-he" }
hostname_director       = "versa-director-1"
hostname_van            = ["versa-analytics-1", "versa-analytics-2", "versa-analytics-3", "versa-analytics-4", "versa-analytics-5", "versa-analytics-6" ]
hostname_vlc            = ["versa-vlc-1", "versa-vlc-2", "versa-vlc-3", "versa-vlc-4", "versa-vlc-5", "versa-vlc-6" ]
machine_type_dir        = "n1-standard-4"
machine_type_van        = "n1-standard-4"
machine_type_controller = "n1-standard-4"
machine_type_svnf       = "n1-standard-4"	
manage                  = ["10.21.1.11", "10.21.1.12", "10.21.1.13", "10.21.1.14", "10.21.1.15", "10.21.1.16", "10.21.1.17", "10.21.1.18", "10.21.1.19", "10.21.1.20", "10.21.1.21", "10.21.1.22", "10.21.1.23", "10.21.1.24", "10.21.1.25"]
sb_segment              = ["10.21.2.11", "10.21.2.12", "10.21.2.13", "10.21.2.14", "10.21.2.15", "10.21.2.16", "10.21.2.17", "10.21.2.18", "10.21.2.19", "10.21.2.20", "10.21.2.21", "10.21.2.22", "10.21.2.23", "10.21.2.24" ]
van_segment             = ["10.21.3.11", "10.21.3.12", "10.21.3.13", "10.21.3.14", "10.21.3.15", "10.21.3.16", "10.21.3.17", "10.21.3.18", "10.21.3.19", "10.21.3.20", "10.21.3.21", "10.21.3.22"  ]
svnf_svnf               = ["10.21.4.11"]
svnf_ctrl               = ["10.21.5.11", "10.21.5.12"]
internet                = ["10.21.6.11"]
wan                     = ["10.21.7.11"]
interconnect            = ["10.41.9.11"]
dr_vd_mgmt              = ["10.41.1.11"]
dr_vd_interconnect      = ["10.21.9.11"]