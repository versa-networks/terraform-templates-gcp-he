https://docs.versa-networks.com/Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Google_Cloud_Platform#elm-main-content


# Install on Google Cloud Platform

1.  <dl class="mt-last-updated-container">

    <dt class="mt-last-updated-label">Last updated</dt>

    <dd><span class="mt-last-updated" data-timestamp="2021-10-21T15:50:04Z">Oct 21, 2021</span></dd>

    </dl>

</header>
</nav>

<div class="elm-related-articles-container">

_![Versa-logo-release-icon.png](./Versa-logo-release-icon.png)For Releases 16.1R2 and later._

To install the Versa headend components on the Google Cloud Platform in standalone mode, you upload the Versa software images to the Google Cloud portal, and then you use the Terraform templates to create virtual machines (VMs) for the Versa headend components. You obtain the Terraform template files the public repository hosted by Versa Networks, at [https://gitlab.com/versa-networks/terraform-templates-gcp-he](https://gitlab.com/versa-networks/terraform-templates-gcp-he "https://gitlab.com/versa-networks/terraform-templates-gcp-he") (for headend deployments) and [https://gitlab.com/versa-networks/devops/-/tree/master/terraform/automation/GCP](https://gitlab.com/versa-networks/devops/-/tree/master/terraform/automation/GCP "https://gitlab.com/versa-networks/devops/-/tree/master/terraform/automation/GCP") (for branch deployments).

<div mt-section-origin="Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Google_Cloud_Platform" class="mt-section" id="section_1"><span id="Create_VMs_for_Versa_Headend_Components"></span>

## Create VMs for Versa Headend Components

This section describes how to use Terraform templates to automatically create VMs for Versa headend components on Google Cloud Platform.

<div mt-section-origin="Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Google_Cloud_Platform" class="mt-section" id="section_2"><span id="Before_You_Begin"></span>

### Before You Begin

*   Install Terraform on your system. For information, see the [Download Terraform](https://www.terraform.io/downloads.html "https://www.terraform.io/downloads.html") article on the Terraform website.
*   Set up Terraform access to Google cloud service account to enable Terraform to provision resources into Google Cloud Platform. For information, see the [Google Cloud Platform](https://cloud.google.com/iam/docs/creating-managing-service-accounts#creating "https://cloud.google.com/iam/docs/creating-managing-service-accounts#creating") articles on the Google cloud website.
*   Create a service account in Google cloud account.
*   Create service account keys in JSON file format for the service account created. For information, see the [Create Service Account Keys](https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys "https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys") article on the Google cloud website.
*   Obtain the JSON file for the service account to authenticate Google cloud platform for logging in to Terraform.
*   Obtain images for the Versa headend components—Versa Director, Versa Analytics, and Versa Controller—from Versa Networks Customer Support.

The following figure illustrates the standalone headend topology created by the Terraform template. The Terraform template provisions networks and Versa headend instances, and it assigns networks to the instances.

*   For MGMT_NETWORK, the management IP address is assigned using this network. The public IP addresses assigned are associated with the three ports to the three Versa headend components.
*   For Director-Controller-VAN_Network, the Director southbound, Controller northbound, and Analytics southbound IP addresses are assigned using this network.
*   For Controller-Branch_Network, the Controller southbound IP address is assigned using this network. This IP address is used to connect to the branch. The public IP address is also assigned to the Controller WAN port.

![Topology_Versa_HE_Standalone_GCP.JPG](./Topology_Versa_HE_Standalone_GCP.jpeg)

</div>

<div mt-section-origin="Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Google_Cloud_Platform" class="mt-section" id="section_3"><span id="Create_the_VMs"></span>

### Create the VMs

To create VMs using Terraform templates:

1.  Contact Versa Customer Support to obtain the Terraform template. When requesting the template, specify whether you are using a standalone or a redundant headend topology.
2.  When you receive the folder that contains the template files, save them to the local system on which Terraform is installed. The template folder contains the files needed to deploy the Versa headend VM resources on Google Cloud Platform. The table at the end of this section describes each of the files.
3.  If you want to make changes to any of the template files, for example, if you want to run another instance of Terraform, make a copy of the original folder and make your changes in the copy. It is recommended that you do not make any changes to the files in the original template folder.
4.  Open a console window on the local system where Terraform was installed. Go to the folder where all the required files are placed from the console window.
5.  Initialize Terraform. The initialization process downloads the Terraform plugins that are required to run the template.

<pre class="mt-indent-1">~$ terraform init
</pre>

![terraform-init.png](./terraform-init.png) 

1.  Display all the resources provisioned as part of the template.

<pre class="mt-indent-1">~$ terraform plan
</pre>

![terraform-plan.png](./terraform-plan.png)

1.  Run the template to deploy all the VM resources on Google Cloud Platform.

<pre class="mt-indent-1">~$ terraform apply
</pre>

![terraform-apply.png](./terraform-apply.png)

The following table describes the content of each Terraform template file and the actions performed by each file.

<table>

<thead>

<tr>

<th scope="col">****Filename****</th>

<th scope="col">****Description or Action****</th>

</tr>

</thead>

<tbody>

<tr>

<td>main.tf</td>

<td>

*   Provision four virtual networks (VPCs). To change the IP prefix, edit the terraform.tfvars file.
*   Provision four subnetworks, one in each VPC with the IP CIDR range provided in the variable file. To change the subnets, edit the terraform.tfvars file.
    *   10.231.1.0/24 subnet is for management of all the headend instances.
    *   10.231.2.0/24 subnet is the control network. This subnetwork is used for Director downstream (southbound) ports, Controller upstream (northbound) ports, and Analytics downstream (southbound ports.
    *   10.231.3.0/24 subnet is the WAN network. This subnetwork is used for Controller downstream (southbound) ports and branch connectivity.
    *   10.231.4.0/24 subnet is the LAN network. This subnetwork is used for branch and client connectivity.
*   Assign a public IP address on the management port of all management port instances.
*   Assign a static public IP address for the Controller WAN port.
*   Provision a route (a user-defined route) for the Controller address to use as a gateway for the IPSec overlay network address. This route is used to send Netconf traffic originating from the Director.
*   Provision a network security group and add all the firewall rules required to set up the headend.
*   Install a Director instance and run the cloud-init script to:
    *   Update the /etc/network/interface file.
    *   Update the /etc/hosts and /etc/hostname file.
    *   Add the SSH key for the Administrator user.
    *   Generate new certificates.
    *   Run the vnms-startup script in non-interactive mode.
*   Install a Controller instance and run the cloud-init script to:
    *   Update the /etc/network/interface file.
    *   Add the SSH key for the Administrator user
*   Install an Analytics instance and run the cloud-init script to:
    *   Update the /etc/network/interface file.
    *   Update the /etc/hosts and /etc/hostname file.
    *   Add the SSH key for the Versa user.

</td>

</tr>

<tr>

<td>var.tf</td>

<td>Provide definitions of all variables defined and used in the template. Do not make any changes to this file.</td>

</tr>

<tr>

<td>terraform.tfvars</td>

<td>

User-defined input variables that are used to populate the Terraform templates. Edit this file to set or change the following variables:

*   credentials_file—Credential file path obtained as part of the initial setup process for the service account.
*   project_id—Google cloud project identifier in which to deploy the headend.
*   region—Region where the user wants to deploy Versa headend setup.
*   zone—Zone from the selected region to create all the resources that belong to Versa headend setup.
*   ssh_key—SSH public key. This key is required to log in to the headend instances. To generate the SSH key, use the sshkey-gen or putty key generator command.
*   ip_cidr_range—Address space information to create the subnetwork in Google in each network. By default, four VPCs and four subnets are created with address space 10.231.1.0/24, 10.231.2.0/24, 10.231.3.0/24, and 10.231.4.0/24.
*   destination_ip_range—Destination IP range to add a custom route to use controller address as a gateway for the IPSec overlay network address for the netconf traffic originates from Director instance. This is an SD-WAN overlay route.
*   labels—Labels and tags to add to the instances.
*   versa_director_image—Filrname of the Director image.
*   versa_flexvnf_image—Filename of the Controller image.
*   versa_van_image—Filename of the Analytics image.
*   hostname_director—Hostname of the Director instance. The default is versa-director-gce.
*   hostname_analytics—Hostname of the Analytics instance. The default is versa-Analytics-gce.
*   machine_type_dir—Instance type and size used to provision the Director instance. The default is n1-standard-4.
*   machine_type_controller—Instance type and size used to provision the Controller instance. The default is n1-standard-4.
*   machine_type_van—Instance type and size used to provision the Analytics instance. The default is n1-standard-4.

</td>

</tr>

<tr>

<td>output.tf</td>

<td>Output parameters, including instance ID and public IP address, for all instances. Do not make any changes to this file.</td>

</tr>

<tr>

<td>director.sh</td>

<td>Bash script that runs as part of cloud-init script on the Versa Director instance. Do not make any changes to this file.</td>

</tr>

<tr>

<td>controller.sh</td>

<td>Bash script that runs as part of cloud-init script on Versa Controller instance. Do not make any changes to this file.</td>

</tr>

<tr>

<td>van.sh</td>

<td>Bash script that runs as part of cloud-init script on Versa Analytics instance. Do not make any changes to this file.</td>

</tr>

<tr>

<td>credentials.json</td>

<td>Authentication information. Place this file in the same folder where you download all authentication files.</td>

</tr>

</tbody>

</table>

</div>

</div>

<div mt-section-origin="Getting_Started/Deployment_and_Initial_Configuration/Headend_Deployment/Installation/Install_on_Google_Cloud_Platform" class="mt-section" id="section_4"><span id="Software_Release_Information"></span>

</nav>

</footer>

</main>
